/*
  Description:
    Wifi functions by Boat Alarm client.
    This is the implementation for the CC3000 WiFi shield.

  Method BoatWiFi::init (called only once)
    Initialize the CC3000
    
  Method BoatWiFi::open
    Start the CC3000
    Connect to Access Point
    Obtain IP address via DHCP

  Method BoatWiFi::httpGet
    Issue HTTP GET request
    Read response
    Close HTTP connection

  Method BoatWiFi::close
    Disconnect from WiFi
    Stop the CC3000

  License:
    Copyright (C) 2015-2016 Alan Noble.

    This file is part of Boat Alarm. Boat Alarm is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Boat Alarm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Boat Alarm in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.

  Revision History:
    28 Nov 2015: Created
*/

#include "boatwifi.h"

#ifdef CC3000
// Interrupt pin must be an interrupt pin
#define ADAFRUIT_CC3000_IRQ   3
// Control pins can be any two pins
#define ADAFRUIT_CC3000_VBAT  5
#define ADAFRUIT_CC3000_CS    10
// Use hardware SPI for the remaining pins
// On an Uno, SCK = 13, MISO = 12, and MOSI = 11

#define MAX_RESOLVE_ATTEMPTS 10
#define MAX_CONNECT_ATTEMPTS 10

extern void wlan_start(UINT16);
extern Print* CC3KPrinter;

BoatWiFi::BoatWiFi(bool debug) : _shield(ADAFRUIT_CC3000_CS, ADAFRUIT_CC3000_IRQ, ADAFRUIT_CC3000_VBAT, SPI_CLOCK_DIV2) {
  _debug = debug;
  _started = false;
  if (!_debug) {
    CC3KPrinter = 0; // suppress CC3000 writing to Serial
  }
}

void BoatWiFi::init() {
  // initialise Wifi Shield - call once
  if (_debug) Serial.println(F("Initializing WiFi..."));
  if (!_shield.begin()) {
    if (_debug) Serial.println(F("Failed to initialize"));
    return;
  }
  _started = true;
}

bool BoatWiFi::open(const char* ssid, const char* key, Security security) {
  // restart Wifi Shield if necessary
  if (!_started) {
    if (_debug) Serial.println(F("Starting WiFi..."));
    wlan_start(0); // Adafruit_CC3000 does not expose a start method
    _started = true;
  }
  
  if (!_shield.connectToAP(ssid, key, security)) {
    if (_debug) Serial.println(F("Failed to connect to WiFi"));
   _shield.stop();  
    return false;
  }

  if (_debug) Serial.println(F("Requesting DHCP..."));
  while (!_shield.checkDHCP()) {
    delay(100); // ToDo: Insert a DHCP timeout!
  }
  if (_debug) Serial.println(F("Obtained DHCP IP address"));
  
  return true;
}

void BoatWiFi::close() {
  if (_debug) Serial.println(F("Disconnecting\n"));
  _shield.disconnect();
  _shield.stop(); // stop the CC3000 to reduce power consumption
  _started = false;
}

bool BoatWiFi::getHostByName(const char*hostname, uint32_t* ip_addr) {
  // resolve hostname to (IPv4) IP address (DNS lookup)  
  *ip_addr = 0;
  if (_debug) Serial.print(F("Resolving ")), Serial.println(hostname);
  for (int attempts = 0; attempts < MAX_RESOLVE_ATTEMPTS; attempts++) {
    if (_shield.getHostByName((char*)hostname, ip_addr) != 0) {
      break;
    }
    delay(500);
  }
  if (ip_addr == 0) {
    if (_debug) Serial.println(F("Couldn't resolve"));
    return false;
  }
  _shield.printIPdotsRev(*ip_addr);
  if (_debug) Serial.println();
  return true;
}

bool BoatWiFi::httpGet(const char* hostname, const char* path, uint32_t ip_addr) {
  // connect to web server (host)
  for (int attempts = 0; attempts < MAX_CONNECT_ATTEMPTS; attempts++) {
    _www = _shield.connectTCP(ip_addr, 80);
    if (_www.connected()) {
      break;
    }
    delay(100);
  }
  if (!_www.connected()) {
    if (_debug) Serial.println(F("Connection failed"));    
    return false;
  }
  if (_debug) Serial.println(F("Connected on port 80"));    

  _www.fastrprint(F("GET "));
  _www.fastrprint(path);
  _www.fastrprint(F(" HTTP/1.1\r\n"));
  _www.fastrprint(F("Host: ")); _www.fastrprint(hostname); _www.fastrprint(F("\r\n"));
  _www.fastrprint(F("Connection: close\r\n"));
  _www.fastrprint(F("\r\n"));
  _www.println();

  if (_debug) Serial.println(F("Sent GET request; checking for response..."));
  while (_www.connected()) {
    while (_www.available()) {
      char cc = _www.read();
      if (_debug) Serial.print(cc);
    }
  }
  if (_debug) Serial.println();

  _www.close();
  return true;
}
#endif
