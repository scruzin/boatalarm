/*
  Description:
    Wifi functions by Boat Alarm client.
    The current implementation is for the CC3000 WiFi shield.

  License:
    Copyright (C) 2015-2016 Alan Noble.

    This file is part of Boat Alarm. Boat Alarm is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Boat Alarm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Boat Alarm in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.

  Revision History:
    28 Nov 2015: Created
*/

#ifndef BOATWIFI_H
#define BOATWIFI_H

#ifdef CC3000
#include <Adafruit_CC3000.h>
#elif ESP8266
#include <ESP8266WiFi.h>
#else
#error BoatAlarm currently supports only CC3000 and ESP8266.
#endif

class BoatWiFi {
 public:
   typedef enum {
#ifdef CC3000
    NoSecurity = WLAN_SEC_UNSEC,
    WepSecurity = WLAN_SEC_WEP,
    WpaSecurity = WLAN_SEC_WPA,
    Wpa2Security = WLAN_SEC_WPA
#elif ESP8266
    AnySecurity = 1
#endif
  } Security;

  BoatWiFi(bool debug);
  void init(void);
  bool open(const char* ssid, const char* key, Security security);
  void close(void);

  bool getHostByName(const char*hostname, uint32_t* ip_addr);
  bool httpGet(const char* hostname, const char* path, uint32_t ip_addr);
  
 private: 
  bool _debug;
#ifdef CC3000
  bool _started;
  Adafruit_CC3000 _shield;
  Adafruit_CC3000_Client _www;
#elif ESP8266
  WiFiClient _www;
#endif
};

#endif
