/*
  Name:
    Boat Alarm - an Arduino program to monitor boat conditions and send alarms.

  Description:
    This is the client part of Boat Alarm, which is responsible for
    monitoring vessel vital signs and sending alarms to the
    service. By default, the client works with the the
    boatalarm.appspot.com service. You must sign up for a Boat Alarm
    key first by going to
    http://boatalarm.appspot.com/register. Registration is free.

    Alarms are HTTP GET methods with the following format.

    /alarm?k=KEY&v=VESSEL&a=ALARM&ad=ALARM_DATA&ac=ACTION&acd=ACTION_DATA

    Alarms are colon-separated strings. Currently implemented alarms are:

    Power:ShorePowerLoss
    Power:BatteryVoltageLow
  
    Note that alarms without a valid key are ignored by the service.

    The client uses a WiFi connection to reach the Internet. This
    could be an old phone running in tethering mode, or your boat's
    access point (if you have one).

    See http://boatalarm.appspot.com for details.

  License:
    Copyright (C) 2015-2016 Alan Noble.

    This file is part of Boat Alarm. Boat Alarm is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Boat Alarm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Boat Alarm in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.

  Revision History:
    28 Nov 2015: Created
    08 May 2016: Added support for ESP8266
    11 May 2016: Added support for monitoring digital pins
    19 May 2016: More flexible alarm pin names
*/

#include <stdlib.h>
#include <limits.h>
#include <string.h>

#define ESP8266 1
#include "boatwifi.h"

// CONFIG: configure the following and the array of alarms further below
#define WLAN_SSID         "Your WiFi SSID"             // your WiFi network SSID
#define WLAN_KEY          "Your WiFi key"              // your WiFi network key
#define WLAN_SECURITY     BoatWiFi::AnySecurity        // your WiFi network's security
#define BA_VESSEL         "Your vessel name"           // Boat Alarm vessel name
#define BA_KEY            "Your Boat Alarm key"        // Boat Alarm key
#define EMAIL_DATA        "your@email.address:Message" // your email address and an optional message

// leave the following unless you're using a different alarm service
#define VERSION           "1.2"
#define SERVICE_HOST      "boatalarm.appspot.com"
#define SERVICE_PATH      "/alarm"
#define EMAIL_ACTION      "email"
#define MAX_PATH          256
#define MONITOR_PERIOD    300 // seconds
#define RETRY_PERIOD      10  // seconds
#define POLL_PERIOD       3600 // seconds
#define ANALOG_TOLERANCE  50  // ~5% of 1023
#ifdef CC3000
#define ALARM_PIN         7
#define NORMAL            LOW
#define ALARMED           HIGH
#elif ESP8266
#define ALARM_PIN         0
#define NORMAL            HIGH
#define ALARMED           LOW
#endif
#define DEBUG             false

typedef struct {
  const char* alarm;    // alarm name, e.g., "Power:BatteryVoltageLow"
  const char* pin;      // name of pin to read, e.g., "AO" or "D2"
  bool report;          // true to report the measured value
  int expected;         // expected value
  int low_threshold;    // low threshold trigger in millivolts (analog only)
  int high_threshold;   // high threshold trigger in millivolts (analog only)
  double scale;         // scale (analog only)
  int value;            // last value measured
  int millivolts;       // millivolts  (analog only)
} Alarm;

// CONFIG: define your alarms here
// Use INT_MAX for high_threshold when you only care about a low threshold, 
// and -1 for low_threshold when you only care about a high threshold
// 16.0 assumes a 16:1 voltage divider on A0
Alarm alarms[] = {{ "Power:ShorePowerLoss", "D2", false, HIGH },
                  { "Power:BatteryVoltageLow", "A0", true, 12600, 12000, INT_MAX, 16.0 }}; 

int nAlarms = sizeof(alarms) / sizeof(Alarm);
char path[MAX_PATH];
BoatWiFi wifi(DEBUG);
uint32_t IPaddress = 0;
unsigned long pollTime = 0;
bool changed = false;
bool poll = true;

// format an integer as a string, returning its length
size_t itoa(int ii, char* buf) {
  char const digits[] = "0123456789";
  char* bufp = buf;
  int tmp = ii;
  do {
    bufp++;
    tmp = tmp / 10;
  } while (tmp);
  *bufp = '\0';
  size_t len = bufp - buf;
  do {
    bufp--;
    *bufp = digits[ii % 10];
    ii = ii / 10;
  } while (ii);
  return len;
}

// Alarm utilities
bool isDigital(Alarm alarm) {
  return alarm.pin[0] == 'D';
}

bool isAnalog(Alarm alarm) {
  return alarm.pin[0] == 'A';
}

int readPin(Alarm alarm) {
  int pin = atoi(alarm.pin + 1);
  if (isDigital(alarm)) {
    return digitalRead(pin);
  }
  // return average of 10 rapid measurements
  int sum = 0;
  for (int jj = 0; jj < 10; jj++) {
    sum += analogRead(pin);
    delay(10);
  }
  return sum/10;
}

char * formatAlarm(Alarm alarm, char* buf) {
  char* bufp = buf;
  strcpy(buf, alarm.pin);
  bufp += strlen(buf);
  *bufp++ = ',';
  if (isDigital(alarm)) {
    *bufp++ = alarm.value ? 'H' : 'L';
  } else {
    bufp += itoa(alarm.millivolts, bufp);
    *bufp++ = 'm';
    *bufp++ = 'V';
  }
  *bufp = '\0';
  return buf;
} 

void setup(void) {
  Serial.begin(115200);
  delay(2000);
  if (DEBUG) Serial.print(F("Boat Alarm v")), Serial.println(VERSION);
  // initialize pin modes
  for (int ii = 0; ii < nAlarms; ii++) {
    if (isDigital(alarms[ii])) {
      int pin = atoi(alarms[ii].pin + 1);
      pinMode(pin, INPUT);
    }
  }
  pinMode(ALARM_PIN, OUTPUT);
  wifi.init();
}

void loop(void) {
  unsigned long currentTime = millis() / 1000;
  char buf[20];
 
  if (currentTime < pollTime) {
    pollTime = 0;  // we rolled over so reset time 
  }
  if ((currentTime - pollTime) >= POLL_PERIOD) {
    poll = true;
    pollTime += POLL_PERIOD;
  }

  if (!changed && !poll) {
    // compare values, unless we already detected a change or it's time to poll again
    if (DEBUG) Serial.println(F("Comparing..."));
    for (int ii = 0; !changed && ii < nAlarms; ii++) {
      int value = readPin(alarms[ii]);
      if ((isDigital(alarms[ii]) && alarms[ii].value != value) || (isAnalog(alarms[ii]) && abs(alarms[ii].value - value) > ANALOG_TOLERANCE)) {
        if (DEBUG) Serial.println(F("Change detected!"));
        changed = true;
      }
    }
    if (!changed) {
      delay(MONITOR_PERIOD * (long)1000);
      return;
    }
  }

  // we open the WiFi connection each time, since there's no point staying connected the whole time
  if (!wifi.open(WLAN_SSID, WLAN_KEY, WLAN_SECURITY)) {
    delay(RETRY_PERIOD * (long)1000); // try again shortly
    return;
  }
  // but we only resolve the host name once
  if (IPaddress == 0 && !wifi.getHostByName(SERVICE_HOST, &IPaddress)) {
    wifi.close();
    delay(RETRY_PERIOD * (long)1000);
    return;
  }

  for (int ii = 0; ii < nAlarms; ii++) {
    alarms[ii].value = readPin(alarms[ii]);
    if (isAnalog(alarms[ii])) {
       alarms[ii].millivolts = (int)((double)(alarms[ii].value) * alarms[ii].scale * 1000 / 1023);
    }
  }

  if (poll) {
    // we poll upon startup and thereafter periodically
    if (DEBUG) Serial.println(F("Polling..."));
    for (int ii = 0; ii < nAlarms; ii++) {
      formatAlarm(alarms[ii], buf);
      sprintf(path, "%s?k=%s&v=%s&a=:Poll&ad=%s,%ldhrs", SERVICE_PATH, BA_KEY, BA_VESSEL, formatAlarm(alarms[ii], buf), currentTime/3600);
      if (!wifi.httpGet(SERVICE_HOST, path, IPaddress)) {
        wifi.close();
        delay(RETRY_PERIOD * (long)1000);
        return;
      } 
    }
    poll = false;
  }

  // check each alarm condition
  if (DEBUG) Serial.println(F("Checking..."));
  bool triggered = false;
  for (int ii = 0; ii < nAlarms; ii++) {
    formatAlarm(alarms[ii], buf);
    if ((isDigital(alarms[ii]) && alarms[ii].value == alarms[ii].expected) || 
        (isAnalog(alarms[ii]) && alarms[ii].millivolts >= alarms[ii].low_threshold && alarms[ii].millivolts <= alarms[ii].high_threshold)) {
      if (DEBUG) {
        Serial.print(buf), Serial.println(F(" OK"));
      }
    } else {
      triggered = true;
      if (DEBUG) {
        Serial.print(buf), Serial.println(F(" FAIL"));
        Serial.print(F("Triggered ")), Serial.println(alarms[ii].alarm);
      }
      if (alarms[ii].report) {
        sprintf(path, "%s?k=%s&v=%s&a=%s&ad=%s&ac=%s&acd=%s",
                SERVICE_PATH, BA_KEY, BA_VESSEL, alarms[ii].alarm, buf, EMAIL_ACTION, EMAIL_DATA);
      } else {
        sprintf(path, "%s?k=%s&v=%s&a=%s&ac=%s&acd=%s",
                SERVICE_PATH, BA_KEY, BA_VESSEL, alarms[ii].alarm, EMAIL_ACTION, EMAIL_DATA);
      }
      if (!wifi.httpGet(SERVICE_HOST, path, IPaddress)) {
        wifi.close();
        delay(RETRY_PERIOD * (long)1000);
        return;
      } 
    }
  }
  if (triggered) {
    digitalWrite(ALARM_PIN, ALARMED);
  } else {
    digitalWrite(ALARM_PIN, NORMAL);
  }

  wifi.close();
  changed = false;
  delay(MONITOR_PERIOD * (long)1000);
}
