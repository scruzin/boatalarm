/*
  Description:
    Wifi functions by Boat Alarm client.
    This is the implementation for the ESP8266.

  Method BoatWiFi::init (called only once)
    Not currently used for ESP8266.
    
  Method BoatWiFi::open
    Connect to Access Point
    Obtain IP address via DHCP

  Method BoatWiFi::httpGet
    Issue HTTP GET request
    Read response
    Close HTTP connection

  Method BoatWiFi::close
    Not currently used for ESP8266.

  License:
    Copyright (C) 2015-2016 Alan Noble.

    This file is part of Boat Alarm. Boat Alarm is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Boat Alarm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Boat Alarm in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.

  Revision History:
    8 May 2016: Created
*/

#include "boatwifi.h"

#ifdef ESP8266
#define MAX_CONNECT_ATTEMPTS 10

BoatWiFi::BoatWiFi(bool debug) {
  _debug = debug;
}

void BoatWiFi::init() {
  // not used
}

bool BoatWiFi::open(const char* ssid, const char* key, Security security) {
  // ESP8266 does not require security
  if (_debug) Serial.print(F("Requesting DHCP."));
  WiFi.begin(ssid, key);
  delay(500);
  
  for (int attempts = 0; WiFi.status() != WL_CONNECTED && attempts < MAX_CONNECT_ATTEMPTS; attempts++) {
    delay(500);
    if (_debug) Serial.print(F("."));
  }

  if (WiFi.status() != WL_CONNECTED) {
    if (_debug) Serial.println(F("Failed to connect to WiFi"));
    return false;
  }
 
  if (_debug) {
    Serial.println(F(""));
    Serial.print(F("Obtained DHCP IP address "));
    Serial.println(WiFi.localIP());
  }
  
  return true;
}

void BoatWiFi::close() {
  // not used
}

bool BoatWiFi::getHostByName(const char*hostname, uint32_t* ip_addr) {
  // not used
  return true;
}

bool BoatWiFi::httpGet(const char* hostname, const char* path, uint32_t ip_addr) {
  if (_debug) Serial.print(F("Connecting to ")), Serial.println(hostname);
  for (int attempts = 0; !_www.connect(hostname, 80) &&attempts < MAX_CONNECT_ATTEMPTS; attempts++) {
    delay(500);
  }

  if (!_www.connected()) {
    if (_debug) Serial.println(F("Connection failed"));    
    return false;
  }
  if (_debug) Serial.println(F("Connected on port 80"));    

  _www.print(String("GET ") + path + " HTTP/1.1\r\nHost: " + hostname + "\r\nConnection: close\r\n\r\n");
  if (_debug) Serial.println(F("Sent GET request; checking for response..."));
  delay(500);
  while (_debug && _www.available()) {
    String line = _www.readStringUntil('\n');
    Serial.println(line);
  }

  return true;
}
#endif
